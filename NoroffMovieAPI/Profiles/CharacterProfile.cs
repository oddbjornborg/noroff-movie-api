﻿using AutoMapper;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Models.DTO.CharacterDTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<CharacterCreateDTO, Character>();

            CreateMap<Character, CharacterReadDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(c => c.Movies.Select(m => m.Id).ToList()));

            CreateMap<CharacterUpdateDTO, Character>();
        }
    }
}
