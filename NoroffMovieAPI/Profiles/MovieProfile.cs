using System;
using AutoMapper;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Models.DTO.Movie;
using NoroffMovieAPI.Models.DTO.MovieDTO;

namespace NoroffMovieAPI.Profiles
{
    public class MovieProfile : Profile
    {
        public MovieProfile()
        {

            CreateMap<MovieCreateDTO, Movie>();

            CreateMap<MovieUpdateDTO, Movie>();

            CreateMap<Movie, MovieReadDTO>()
                .ForMember(dto => dto.Characters, opt => opt
                .MapFrom(m => m.Characters.Select(c => c.Id).ToList()));
        }
    }
}

