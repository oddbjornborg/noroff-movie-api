﻿using System;
using AutoMapper;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Models.DTO.CharacterDTO;
using NoroffMovieAPI.Models.DTO.FranchiseDTO;

namespace NoroffMovieAPI.Profiles
{
    internal class FranchiseProfile : Profile
    {
        public FranchiseProfile()
        {
            CreateMap<FranchiseCreateDTO, Franchise>();

            CreateMap<Franchise, FranchiseReadDTO>()
                .ForMember(dto => dto.Movies, opt => opt
                .MapFrom(f => f.Movies.Select(m => m.Id).ToList()));

            CreateMap<FranchiseUpdateDTO, Franchise>();
        }
    }
}
