using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;
using NoroffMovieAPI.Models;
using NoroffMovieAPI.Services.Characters;
using NoroffMovieAPI.Services.Franchises;
using NoroffMovieAPI.Services.Movies;
using System.Reflection;

var builder = WebApplication.CreateBuilder(args);

var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);

builder.Services.AddControllers();
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(options =>
{
    options.SwaggerDoc("v1", new OpenApiInfo
    {
        Version = "v1",
        Title = "Movie Characters API",
        Description = "Simple API to manage and view characters in movies",
        Contact = new OpenApiContact
        {
            Name = "Oddbj�rn Borge-Jensen",
            Url = new Uri("https://gitlab.com/oddbjornborg")
        },
        License = new OpenApiLicense
        {
            Name = "MIT 2022",
            Url = new Uri("https://opensource.org/licenses/MIT")
        }
    });
    options.IncludeXmlComments(xmlPath);
});

// Adding services to the container
builder.Services.AddDbContext<MovieCharactersDbContext>(
    opt => opt.UseSqlServer(
        builder.Configuration.GetConnectionString("MovieDb")
        )
    );


// Automapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());

// Adding logging through ILogger
builder.Host.ConfigureLogging(logging =>
{
    logging.ClearProviders();
    logging.AddConsole();
});

// Custom Services
builder.Services.AddTransient<ICharacterService, CharacterServiceImpl>();
builder.Services.AddTransient<IFranchiseService, FranchiseServiceImpl>();
builder.Services.AddTransient<IMovieService, MovieServiceImpl>();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
