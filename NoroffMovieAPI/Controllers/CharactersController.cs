﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Models.DTO.CharacterDTO;
using NoroffMovieAPI.Services.Characters;
using NoroffMovieAPI.Util.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Controllers
{
    [Route("api/v1/character")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class CharactersController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ICharacterService _characterService;

        public CharactersController(IMapper mapper, ICharacterService characterService)
        {
            _mapper = mapper;
            _characterService = characterService;
        }

        /// <summary>
        /// Create a new character
        /// </summary>
        /// <param name="characterDTO"></param>
        /// <returns>Newly created character</returns>
        [HttpPost]
        public async Task<ActionResult> CreateCharacter(CharacterCreateDTO characterDTO)
        {
            Character character = _mapper.Map<Character>(characterDTO);
            await _characterService.AddAsync(character);
            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        /// <summary>
        /// Gets all characters in the database with movies represented by ID
        /// </summary>
        /// <returns>List of CharacterReadDTO</returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterReadDTO>>> GetCharacters()
        {
            return Ok(
                _mapper.Map<List<CharacterReadDTO>>(
                    await _characterService.GetAllAsync()
                )
            );
        }

        /// <summary>
        /// Gets a single character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>CharacterReadDTO</returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterReadDTO>> GetCharacter(int id)
        {
            try
            {
                return Ok(
                    _mapper.Map<CharacterReadDTO>(
                        await _characterService.GetByIdAsync(id)
                    )
                );
            }
            catch (CharacterNotFoundException e)
            {
                return NotFound(
                    new ProblemDetails()
                    {
                        Detail = e.Message,
                        Status = (int)HttpStatusCode.NotFound
                    }
                );
            }
        }

        /// <summary>
        /// Updates data for an existing character
        /// </summary>
        /// <param name="id"></param>
        /// <param name="characterDTO"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateCharacter(int id, CharacterUpdateDTO characterDTO)
        {
            if (id != characterDTO.Id) return BadRequest();

            try
            {
                await _characterService.UpdateAsync(_mapper.Map<Character>(characterDTO));
                return NoContent();
            }
            catch (CharacterNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        /// <summary>
        /// Deletes a character from the database
        /// </summary>
        /// <param name="id"></param>
        /// <returns>No content</returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteCharacter(int id)
        {
            try
            {
                await _characterService.DeleteAsync(id);
                return NoContent();
            }
            catch (CharacterNotFoundException e)
            {
                return NotFound(new ProblemDetails() { 
                    Detail = e.Message, 
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }
    }
}
