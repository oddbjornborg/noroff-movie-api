﻿using System;
using System.Net;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Models.DTO.FranchiseDTO;
using NoroffMovieAPI.Models.DTO.Movie;
using NoroffMovieAPI.Services.Franchises;
using NoroffMovieAPI.Util.Exceptions;

namespace NoroffMovieAPI.Controllers
{
    [Route("api/v1/franchise")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class FranchiseController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IFranchiseService _franchiseService;

        public FranchiseController(IMapper mapper, IFranchiseService franchiseService)
        {
            _mapper = mapper;
            _franchiseService = franchiseService;
        }


        [HttpPost]
        public async Task<IActionResult> CreateFranchise(FranchiseCreateDTO franchiseDTO)
        {
            Franchise franchise = _mapper.Map<Franchise>(franchiseDTO);
            await _franchiseService.AddAsync(franchise);
            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise); //WTF?
        }



        [HttpGet]
        public async Task<ActionResult<ICollection<FranchiseReadDTO>>> GetFranchise() //Skal vi bruke Collection IEnumerable?
        {
            return Ok(_mapper.Map<List<FranchiseReadDTO>>(await _franchiseService.GetAllAsync()));
        }




        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseReadDTO>> GetFranchise(int id)
        {
            try
            {
                return Ok(_mapper.Map<FranchiseReadDTO>(await _franchiseService.GetByIdAsync(id)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails() { Detail = e.Message, Status = ((int)HttpStatusCode.NotFound) });
            }
        }




        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateFranchise(int id, FranchiseUpdateDTO franchiseDTO)
        {
            if (id != franchiseDTO.Id)
                return BadRequest();

            try
            {
                await _franchiseService.UpdateAsync(_mapper.Map<Franchise>(franchiseDTO));
                return NoContent(); //WTF

            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails() { Detail = e.Message, Status = ((int)HttpStatusCode.NotFound) });
            }
        }



        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteFranchise(int id)
        {
            try
            {
                await _franchiseService.DeleteAsync(id);
                return NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails { Detail = e.Message, Status = ((int)HttpStatusCode.NotFound) });
            }
        }


        
        [HttpGet("{id}/movies")]
        public async Task<ActionResult<ICollection<MovieReadDTO>>> GetMoviesInFranchise(int franchiseId)
        {
            try
            {
                return Ok(_mapper.Map<List<MovieReadDTO>>(await _franchiseService.GetMoviesAsync(franchiseId)));
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails { Detail = e.Message, Status = ((int)HttpStatusCode.NotFound) });
            }
        }


        [HttpPut("{id}/movies")]
        public async Task<IActionResult> UpdateMoviesInFranchise(int[] moviesIds, int franchiseId)
        {
            try
            {
                await _franchiseService.UpdateMoviesAsync(moviesIds, franchiseId);
                return NoContent();
            }
            catch (EntityNotFoundException e)
            {
                return NotFound(new ProblemDetails { Detail = e.Message, Status = ((int)HttpStatusCode.NotFound) });
            }
        }

    }
}

