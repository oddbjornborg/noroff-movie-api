﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Models.DTO.CharacterDTO;
using NoroffMovieAPI.Models.DTO.Movie;
using NoroffMovieAPI.Models.DTO.MovieDTO;
using NoroffMovieAPI.Services.Movies;
using NoroffMovieAPI.Util.Exceptions;
using System.Net;

namespace NoroffMovieAPI.Controllers
{
    [Route("api/v1/movies")]
    [ApiController]
    [ApiConventionType(typeof(DefaultApiConventions))]
    public class MovieController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IMovieService _movieService;

        public MovieController(IMapper mapper, IMovieService movieService)
        {
            _mapper = mapper;
            _movieService = movieService;
        }

        [HttpPost]
        public async Task<ActionResult> CreateMovie(MovieCreateDTO movieCreateDTO)
        {
            Movie movie = _mapper.Map<Movie>(movieCreateDTO);
            await _movieService.AddAsync(movie);
            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieReadDTO>>> GetMovies()
        {
            return Ok(
                _mapper.Map<List<MovieReadDTO>>(
                    await _movieService.GetAllAsync())
                );
        }

        [HttpGet("{id}")]
        public async Task<ActionResult<MovieReadDTO>> GetMovie(int id)
        {
            return Ok(
                _mapper.Map<MovieReadDTO>(
                    await _movieService.GetByIdAsync(id))
                );
        }

        [HttpPut("{id}")]
        public async Task<ActionResult> UpdateMovie(int id, MovieUpdateDTO movieDto)
        {
            if(id != movieDto.Id)
                return BadRequest();

            try
            {
                await _movieService.UpdateAsync(_mapper.Map<Movie>(movieDto));
                return NoContent();
            }
            catch (MovieNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        [HttpDelete("{id}")]
        public async Task<ActionResult> DeleteMovie(int id)
        {
            try
            {
                await _movieService.DeleteAsync(id);
                return NoContent();
            }
            catch (MovieNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }

        [HttpGet("characters/{id}")]
        public async Task<ActionResult<List<Character>>> GetCharacters(int id)
        {
            return Ok(
                _mapper.Map<List<CharacterReadDTO>>(
                    await _movieService.GetCharactersAsync(id))
                );
        }

        [HttpPatch("characters/{id}")]
        public async Task<ActionResult> UpdateCharacters(int[] characters, int id)
        {
            try
            {
                await _movieService.UpdateCharactersAsync(characters, id);
                return NoContent();
            }
            catch (MovieNotFoundException e)
            {
                return NotFound(new ProblemDetails()
                {
                    Detail = e.Message,
                    Status = (int)HttpStatusCode.NotFound
                });
            }
        }
    }
}
