﻿using System;
using Microsoft.EntityFrameworkCore;
using NoroffMovieAPI.Models;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Services.Franchises;
using NoroffMovieAPI.Util.Exceptions;

namespace NoroffMovieAPI.Services.Movies
{
    public class MovieServiceImpl : IMovieService
    {

        private readonly MovieCharactersDbContext _dbContext;
        private readonly ILogger<MovieServiceImpl> _logger;

        public MovieServiceImpl(MovieCharactersDbContext dbContext, ILogger<MovieServiceImpl> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task AddAsync(Movie entity)
        {
            await _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetAllAsync()
        {
            return await _dbContext.Movies
                .Include(m => m.Franchise)
                .Include(m => m.Characters)
                .ToListAsync();
        }

        public async Task<Movie> GetByIdAsync(int id)
        {
            if(!await MovieExistsAsync(id))
            {
                _logger.LogError("Movie not found with Id: " + id);
                throw new MovieNotFoundException();
            }

            return await _dbContext.Movies
                .Include(m => m.Franchise)
                .Include(m => m.Characters)
                .Where(m => m.Id == id)
                .FirstAsync();
        }

        public async Task UpdateAsync(Movie entity)
        {
            if (!await MovieExistsAsync(entity.Id))
            {
                _logger.LogError("Movie not found with Id: " + entity.Id);
                throw new MovieNotFoundException();
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var movie = await _dbContext.Movies.FindAsync(id);

            if(movie == null)
            {
                _logger.LogError("Movie not found with Id: " + id);
                throw new MovieNotFoundException();
            }

            _dbContext.Movies.Remove(movie);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetCharactersAsync(int movieId)
        {
            if(!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Movie not found with Id: " + movieId);
                throw new MovieNotFoundException();
            }

            return await _dbContext.Characters
                .Include(c => c.Movies)
                .Where(c => c.Movies.Any(m => m.Id == movieId))
                .ToListAsync();
        }

        public async Task UpdateCharactersAsync(int[] characterIds, int movieId)
        {
            if(!await MovieExistsAsync(movieId))
            {
                _logger.LogError("Movie not found with Id: " + movieId);
                throw new MovieNotFoundException();
            }

            List<Character> characters = characterIds
                .ToList()
                .Select(cid => _dbContext.Characters
                .Where(c => c.Id == cid).First())
                .ToList();

            Movie movie = await _dbContext.Movies
                .Where(m => m.Id == movieId)
                .FirstAsync();

            movie.Characters = characters;
            _dbContext.Entry(movie).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        private async Task<bool> MovieExistsAsync(int id)
        {
            return await _dbContext.Movies.AnyAsync(m => m.Id == id);
        }
    }
}

