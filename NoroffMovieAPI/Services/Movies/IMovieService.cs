﻿using System;
using NoroffMovieAPI.Models.Domain;

namespace NoroffMovieAPI.Services.Movies
{
    public interface IMovieService : ICrudService<Movie, int>
    {
        Task<ICollection<Character>> GetCharactersAsync(int movieId);
        Task UpdateCharactersAsync(int[] characterIds, int movieId);
    }
}

