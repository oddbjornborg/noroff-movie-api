﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Services
{
    public interface ICrudService<T, ID>
    {
        public Task<ICollection<T>> GetAllAsync();

        Task<T> GetByIdAsync(ID id);

        Task AddAsync(T entity);

        Task UpdateAsync(T entity);

        Task DeleteAsync(ID id);
    }
}
