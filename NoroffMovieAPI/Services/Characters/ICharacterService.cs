﻿using NoroffMovieAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Services.Characters
{
    public interface ICharacterService : ICrudService<Character, int>
    {
        Task<ICollection<Movie>> GetMoviesAsync(int id);
    }
}
