﻿using Microsoft.Build.Framework;
using Microsoft.EntityFrameworkCore;
using NoroffMovieAPI.Models;
using NoroffMovieAPI.Models.Domain;
using NoroffMovieAPI.Models.DTO.CharacterDTO;
using NoroffMovieAPI.Util.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Services.Characters
{
    public class CharacterServiceImpl : ICharacterService
    {
        private readonly MovieCharactersDbContext _dbContext;
        private readonly ILogger<CharacterServiceImpl> _logger;

        public CharacterServiceImpl(MovieCharactersDbContext dbContext, ILogger<CharacterServiceImpl> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task AddAsync(Character entity)
        {
            await _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Character>> GetAllAsync()
        {
            return await _dbContext.Characters
                .Include(c => c.Movies)
                .ToListAsync();
        }

        public async Task<Character> GetByIdAsync(int id)
        {
            // Log and throw error handling
            if (!await CharacterExistsAsync(id))
            {
                _logger.LogError("Character not found with Id: " + id);
                throw new CharacterNotFoundException();
            }
            // Want to include all related data for professor
            return await _dbContext.Characters
                .Where(c => c.Id == id)
                .FirstAsync();
        }

        public async Task DeleteAsync(int id)
        {
            var character = await _dbContext.Characters.FindAsync(id);

            // Log and throw pattern
            if (character == null)
            {
                _logger.LogError("Character not found with Id: " + id);
                throw new CharacterNotFoundException();
            }
            // We set our entities to have nullable relationships
            // so it removes the FKs when delete it.
            _dbContext.Characters.Remove(character);
            await _dbContext.SaveChangesAsync();
        }

        public async Task UpdateAsync(Character entity)
        {
            // Log and throw error handling
            if (!await CharacterExistsAsync(entity.Id))
            {
                _logger.LogError("Character not found with Id: " + entity.Id);
                throw new CharacterNotFoundException();
            }

            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }

        public async Task<ICollection<Movie>> GetMoviesAsync(int id)
        {
            // Log and throw error handling
            if (!await CharacterExistsAsync(id))
            {
                _logger.LogError("Character not found with Id: " + id);
                throw new CharacterNotFoundException();
            }
            // Dont need to include related data because of the DTO we are mapping to.
            // This can change depending on the business requirements.
            return await _dbContext.Movies
                .Where(m => m.Characters.Any(c => c.Id == id))
                .ToListAsync();
        }

        private async Task<bool> CharacterExistsAsync(int id)
        {
            return await _dbContext.Characters.AnyAsync(e => e.Id == id);
        }

    }
}
