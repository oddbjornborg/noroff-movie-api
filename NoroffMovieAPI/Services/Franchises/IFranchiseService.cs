﻿using System;
using NoroffMovieAPI.Models.Domain;

namespace NoroffMovieAPI.Services.Franchises
{
    public interface IFranchiseService : ICrudService<Franchise, int>
    {
        Task<ICollection<Movie>> GetMoviesAsync(int franchiseId);

  
        Task UpdateMoviesAsync(int[] movieIds, int franchiseId);

    }
}

