﻿using System;
using NoroffMovieAPI.Models;
using NoroffMovieAPI.Models.Domain;
using Microsoft.EntityFrameworkCore;
using System.Linq;
using NoroffMovieAPI.Util.Exceptions;

namespace NoroffMovieAPI.Services.Franchises
{
    public class FranchiseServiceImpl : IFranchiseService
    {
        private readonly MovieCharactersDbContext _dbContext;
        private readonly ILogger<FranchiseServiceImpl> _logger;

        public FranchiseServiceImpl(MovieCharactersDbContext dbContext, ILogger<FranchiseServiceImpl> logger)
        {
            _dbContext = dbContext;
            _logger = logger;
        }

        public async Task AddAsync(Franchise entity)
        {
            await _dbContext.AddAsync(entity);
            await _dbContext.SaveChangesAsync();
        }


        public async Task<ICollection<Franchise>> GetAllAsync()
        {
            return await _dbContext.Franchises
                .Include(f => f.Movies)
                .ToListAsync();
        }

        public async Task<Franchise> GetByIdAsync(int id)
        {
            if(!await FranchiseExistsAsync(id))
            {
                _logger.LogError("Franchise not found with Id: " + id);
                throw new FranchiseNotFoundException();
            }

            return await _dbContext.Franchises
                .Where(f => f.Id == id)
                .Include(f => f.Movies)
                .FirstAsync();
        }

        public async Task UpdateAsync(Franchise entity)
        {
            if(!await FranchiseExistsAsync(entity.Id))
            {
                _logger.LogError("Could not update: Franchise not found with Id: " + entity.Id);
                throw new FranchiseNotFoundException();
            }
            _dbContext.Entry(entity).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }


        public async Task DeleteAsync(int id)
        {
            var franchise = await _dbContext.Franchises.FindAsync(id);

            if(franchise == null)
            {
                _logger.LogError("Could not delete: Franchise not found with Id: " + id);
                throw new FranchiseNotFoundException();
            }

            _dbContext.Franchises.Remove(franchise);
            await _dbContext.SaveChangesAsync();
        }

      
        
        public async Task<ICollection<Movie>> GetMoviesAsync(int franchiseId)
        {
            if(!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Could not get movies: Franchise not found with Id: " + franchiseId);
                throw new FranchiseNotFoundException();
            }

            return await _dbContext.Movies
                .Where(m => m.FranchiseId == franchiseId)
                .ToListAsync();
        }

       

        public async Task UpdateMoviesAsync(int[] movieIds, int franchiseId)
        {
            if(!await FranchiseExistsAsync(franchiseId))
            {
                _logger.LogError("Could not update movie: Franchise not found with Id: " + franchiseId);
                throw new FranchiseNotFoundException();
            }

            List<Movie> movies = movieIds
                .ToList()
                .Select(mid => _dbContext.Movies
                .Where(m => m.Id == franchiseId).First())
                .ToList();

            Franchise franchise = await _dbContext.Franchises
                .Where(f => f.Id == franchiseId)
                .FirstAsync();

            franchise.Movies = movies;
            _dbContext.Entry(franchise).State = EntityState.Modified;
            await _dbContext.SaveChangesAsync();
        }


        private async Task<bool> FranchiseExistsAsync(int id)
        {
            return await _dbContext.Franchises.AnyAsync(f => f.Id == id);
        }
    }
}

