﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Models.DTO.CharacterDTO
{
    public class CharacterCreateDTO
    {
        public string FullName { get; set; }
        public string? Alias { get; set; }
        public string Gender { get; set; }
        public string Picture { get; set; }
    }
}
