﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Models.Domain
{ 
    [Table("Movies")]
    public partial class Movie
    {
        public Movie()
        {
            Characters = new HashSet<Character>();
        }

        public int Id { get; set; }

        [MaxLength(100)]
        public string Title { get; set; }

        [MaxLength(50)]
        public string Genre { get; set; }

        public int ReleaseYear { get; set; }

        [MaxLength(50)]
        public string Director { get; set; }

        [MaxLength(500)]
        public string? Picture { get; set; }

        [MaxLength(200)]
        public string? Trailer { get; set; }

        //One movie belongs to one franchise.
        public int? FranchiseId { get; internal set; }
        public virtual Franchise? Franchise { get; set; }

        //One movie contains many characters.
        public virtual ICollection<Character> Characters { get; set; }
        
    }
}
