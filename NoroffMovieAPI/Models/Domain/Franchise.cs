﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace NoroffMovieAPI.Models.Domain
{

    [Table("Franchises")]
    public partial class Franchise
    {
        public Franchise()
        {
            Movies = new HashSet<Movie>();
        }

        public int Id { get; set; }

        [MaxLength(100)]
        public string Name { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }

        //Franchise can contain many movies.
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
