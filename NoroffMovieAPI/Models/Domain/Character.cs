﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Models.Domain
{
    [Table("Character")]
    public partial class Character
    {
        public Character()
        {
            Movies = new HashSet<Movie>();
        }

        public int Id { get; set; }

        [MaxLength(50)]
        public string FullName { get; set; }

        [MaxLength(50)]
        public string? Alias { get; set; }

        [MaxLength(10)]
        public string Gender { get; set; }

        [MaxLength(500)]
        public string Picture { get; set; }

        //Character can play in multiple movies.
        public virtual ICollection<Movie> Movies { get; set; }
    }
}
