﻿using Microsoft.EntityFrameworkCore;
using NoroffMovieAPI.Models.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NoroffMovieAPI.Util.Consts;

namespace NoroffMovieAPI.Models
{
    public class MovieCharactersDbContext : DbContext
    {
        public virtual DbSet<Character> Characters { get; set; } = null!;
        public virtual DbSet<Movie> Movies { get; set; } = null!;
        public virtual DbSet<Franchise> Franchises { get; set; } = null!;

        public MovieCharactersDbContext(DbContextOptions<MovieCharactersDbContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Franchise>()
            .HasData(new Franchise()
            {
                Id = 1,
                Name = "Marvel Cenematic Universe",
                Description = "The Marvel Cinematic Universe (MCU) is an American media franchise and shared universe centered on a series of superhero films produced by Marvel Studios."
            });

            modelBuilder.Entity<Franchise>()
              .HasData(new Franchise()
              {
                  Id = 2,
                  Name = "Star Wars",
                  Description = "Star Wars is an American epic space opera multimedia franchise created by George Lucas, which began with the eponymous 1977 film and quickly became a worldwide pop-culture phenomenon."
              });


            modelBuilder.Entity<Movie>()
               .HasData(new Movie()
               {
                   Id = 1,
                   Title = "Avengers: End Game",
                   Genre = "Superhero",
                   ReleaseYear = 2019,
                   Director = "Joe Russo",
                   Picture = "image",
                   Trailer = "https://www.youtube.com/results?search_query=endgame+trailer",
                   FranchiseId = 1
               });

            modelBuilder.Entity<Movie>()
                .HasData(new Movie()
                {
                    Id = 2,
                    Title = "Spider-Man: No Way Home",
                    Genre = "Superhero",
                    ReleaseYear = 2021,
                    Director = "Jon Watts",
                    Picture = "image",
                    Trailer = "https://www.youtube.com/watch?v=JfVOs4VSpmA",
                    FranchiseId = 1
                });



            modelBuilder.Entity<Movie>()
                .HasData(new Movie()
                {
                    Id = 3,
                    Title = "Star Wars: The Force Awakens",
                    Genre = "Science fiction",
                    ReleaseYear = 2015,
                    Director = "J.J Abrams",
                    Picture = "image",
                    Trailer = "https://www.youtube.com/watch?v=sGbxmsDFVnE",
                    FranchiseId = 2
                });



            modelBuilder.Entity<Character>()
                .HasData(new Character()
                {
                    Id = 1,
                    FullName = "Robert Downey",
                    Alias = "Iron Man",
                    Gender = Gender.MALE,
                    Picture = "image",
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character()
                {
                    Id = 2,
                    FullName = "Chris Evans",
                    Alias = "Captain America",
                    Gender = Gender.MALE,
                    Picture = "image",
                });
            modelBuilder.Entity<Character>()
                .HasData(new Character()
                {
                    Id = 3,
                    FullName = "Tom Holland",
                    Alias = "Spider Man",
                    Gender = Gender.MALE,
                    Picture = "image",
                });

            modelBuilder.Entity<Character>()
                .HasData(new Character()
                {
                    Id = 4,
                    FullName = "Harrison Ford",
                    Alias = "Han Solo",
                    Gender = Gender.MALE,
                    Picture = "image",
                });




            modelBuilder.Entity<Character>()
                .HasMany(p => p.Movies)
                .WithMany(m => m.Characters)
                .UsingEntity<Dictionary<string, object>>(
                    "CharactersMovies",
                    r => r.HasOne<Movie>().WithMany().HasForeignKey("MovieId"),
                    l => l.HasOne<Character>().WithMany().HasForeignKey("CharacterId"),
                    je =>
                    {
                        je.HasKey("CharacterId", "MovieId");
                        je.HasData(
                            new { CharacterId = 1, MovieId = 1 },
                            new { CharacterId = 2, MovieId = 1 },
                            new { CharacterId = 3, MovieId = 1 },
                            new { CharacterId = 3, MovieId = 2 },
                            new { CharacterId = 4, MovieId = 3 }
                        );
                    });

        }
    }
}