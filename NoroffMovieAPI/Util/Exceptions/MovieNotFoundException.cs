﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Util.Exceptions
{
    internal class MovieNotFoundException : EntityNotFoundException
    {
        public MovieNotFoundException() : base("No such movie found") { }
    }
}
