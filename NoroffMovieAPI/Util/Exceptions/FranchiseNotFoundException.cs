﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NoroffMovieAPI.Util.Exceptions
{
    internal class FranchiseNotFoundException : EntityNotFoundException
    {
        public FranchiseNotFoundException() : base("No such franchise found") { }
    }
}
