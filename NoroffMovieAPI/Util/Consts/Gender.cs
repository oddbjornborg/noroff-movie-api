﻿namespace NoroffMovieAPI.Util.Consts
{
    public static class Gender
    {
        public static readonly string MALE = "Male";
        public static readonly string FEMALE = "Female";
        public static readonly string UNKNOWN = "Unknown";
    }
}
